package com.anddevs.perfreview.presentation.main;

import android.os.Bundle;

import com.anddevs.perfreview.R;
import com.anddevs.perfreview.presentation.base.BaseActivity;
import com.anddevs.perfreview.presentation.trending.TrendingFragment;

public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TrendingFragment fragment = new TrendingFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, fragment).commit();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }
}
