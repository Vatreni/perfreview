package com.anddevs.perfreview.presentation.search;

import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.domain.SearchRepository;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by user on 04-Jun-18.
 */

public class SearchViewModel extends ViewModel {

    SearchRepository repository;

    public SearchViewModel(SearchRepository repository) {
        this.repository = repository;
    }

    public Observable<List<Gif>> loadData(String query) {
        Log.d("qqqqq", "loadData: ");
        return repository.search(query);

    }
}
