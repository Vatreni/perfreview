package com.anddevs.perfreview.presentation.di;

import com.anddevs.perfreview.presentation.search.SearchActivity;
import com.anddevs.perfreview.presentation.search.SearchViewModel;

import dagger.Component;

@SearchScope
@Component(modules = SearchModule.class, dependencies = AppComponent.class)
public interface SearchComponent {
    void inject(SearchActivity activity);

    void inject(SearchViewModel viewModel);
}
