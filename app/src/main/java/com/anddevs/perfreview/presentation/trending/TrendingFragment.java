package com.anddevs.perfreview.presentation.trending;

import android.arch.lifecycle.LiveData;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.anddevs.perfreview.R;
import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.presentation.App;
import com.anddevs.perfreview.presentation.DividerItemDecoration;
import com.anddevs.perfreview.presentation.di.DaggerTrendingComponent;
import com.anddevs.perfreview.presentation.di.TrendingModule;
import com.anddevs.perfreview.presentation.search.SearchActivity;

import java.util.List;

import javax.inject.Inject;


/**
 * Created by user on 04-Jun-18.
 */

public class TrendingFragment extends Fragment {
    @Inject
    TrendingViewModel viewModel;

    private RecyclerView rvTrending;
    private TrendingAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        DaggerTrendingComponent.builder()
                .appComponent(App.get(getContext()).getAppComponent())
                .trendingModule(new TrendingModule(this))
                .build().inject(this);
        return inflater.inflate(R.layout.fragment_trending, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvTrending = view.findViewById(R.id.rvTrending);
        rvTrending.addItemDecoration(new DividerItemDecoration(getResources().getDimensionPixelSize(R.dimen.divider)));
        rvTrending.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new TrendingAdapter();
        rvTrending.setAdapter(adapter);

        LiveData<List<Gif>> liveData = viewModel.loadData();
        liveData.observe(this, data -> adapter.setData(data));

//        viewModel.loadData()
//                .subscribe(data -> adapter.setData(data),
//                        Throwable::printStackTrace);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemSearch:
                SearchActivity.start(getContext());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
