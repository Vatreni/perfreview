package com.anddevs.perfreview.presentation.trending;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anddevs.perfreview.data.mapping.GifMapper;
import com.anddevs.perfreview.R;
import com.anddevs.perfreview.data.local.Gif;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 04-Jun-18.
 */

public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.Holder> {

    private List<Gif> data = new ArrayList<>();

    public void setData(List<Gif> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trending, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Gif gif = data.get(position);
        holder.tvTitle.setText(gif.title);
        holder.tvDate.setText( GifMapper.OUTPUT_DATE_FORMAT.format(new Date(gif.date)));
//        holder.tvUsername.setText(gif.author);

//        RequestBuilder<GifDrawable> thumbnailRequest = Glide
//                .with(holder.itemView.getContext())
//                .asGif()
//                .load(datum.getImages().get480wStill().getUrl());

        Log.d("qqqqq", "onBindViewHolder: "  + gif.title + ", " + gif.downsizedUrl);
        Glide.with(holder.itemView.getContext())
                .asGif()
                .load(gif.downsizedUrl)
//                .thumbnail(thumbnailRequest)
                .into(holder.ivGif);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private TextView tvDate;
        private TextView tvUsername;
        private ImageView ivGif;
        private ImageView ivUserAvatar;

        public Holder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTrendingTitle);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvUsername = itemView.findViewById(R.id.tvUsername);
            ivGif = itemView.findViewById(R.id.ivGif);
            ivUserAvatar = itemView.findViewById(R.id.ivAvatar);
        }
    }
}
