package com.anddevs.perfreview.presentation.base;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.anddevs.perfreview.domain.SearchRepository;
import com.anddevs.perfreview.domain.TrendingRepository;
import com.anddevs.perfreview.presentation.search.SearchViewModel;
import com.anddevs.perfreview.presentation.trending.TrendingViewModel;

/**
 * Created by user on 04-Jun-18.
 */

public class ModelFactory extends ViewModelProvider.NewInstanceFactory {

    private TrendingRepository repository;
    private SearchRepository searchRepository;

    public ModelFactory(TrendingRepository repository) {
        super();
        this.repository = repository;
    }

    public ModelFactory(SearchRepository repository) {
        super();
        this.searchRepository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == TrendingViewModel.class) {
            return (T) new TrendingViewModel(repository);
        }
        if (modelClass == SearchViewModel.class) {
            return (T) new SearchViewModel(searchRepository);
        }
        return null;
    }
}
