package com.anddevs.perfreview.presentation.di;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.FragmentActivity;

import com.anddevs.perfreview.data.local.GiphyDatabase;
import com.anddevs.perfreview.data.remote.RestClient;
import com.anddevs.perfreview.presentation.base.ModelFactory;
import com.anddevs.perfreview.domain.SearchRepository;
import com.anddevs.perfreview.presentation.search.SearchViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchModule {

    private FragmentActivity activity;

    public SearchModule(FragmentActivity activity) {
        this.activity = activity;
    }

    @Provides
    public SearchRepository provideRepository(RestClient client, GiphyDatabase database) {
        return new SearchRepository(client, database);
    }

    @Provides
    public SearchViewModel provideTrendingViewModel(SearchRepository repository) {
        return ViewModelProviders.of(activity, new ModelFactory(repository)).get(SearchViewModel.class);
    }
}
