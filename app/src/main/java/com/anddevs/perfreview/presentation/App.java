package com.anddevs.perfreview.presentation;

import android.app.Application;
import android.content.Context;

import com.anddevs.perfreview.presentation.di.AppComponent;
import com.anddevs.perfreview.presentation.di.AppModule;
import com.anddevs.perfreview.presentation.di.DaggerAppComponent;

public class App extends Application {

    protected AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }
}
