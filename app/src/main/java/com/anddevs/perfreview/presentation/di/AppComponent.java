package com.anddevs.perfreview.presentation.di;

import com.anddevs.perfreview.data.local.GiphyDatabase;
import com.anddevs.perfreview.data.remote.RestClient;
import com.anddevs.perfreview.presentation.App;
import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by user on 30-May-18.
 */
@Component(modules = {AppModule.class, RestModule.class, DbModule.class})
@Singleton
public interface AppComponent {

    RestClient getRestClient();

    GiphyDatabase getDatabase();

    void inject(App app);
}
