package com.anddevs.perfreview.presentation.trending;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.domain.TrendingRepository;

import java.util.List;

/**
 * Created by user on 04-Jun-18.
 */

public class TrendingViewModel extends ViewModel {

    TrendingRepository repository;

    public TrendingViewModel(TrendingRepository repository) {
        this.repository = repository;
    }

    public LiveData<List<Gif>> loadData() {
        return repository.getTrending();
    }
}
