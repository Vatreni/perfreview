package com.anddevs.perfreview.presentation.di;

import android.arch.persistence.room.Room;
import android.content.Context;


import com.anddevs.perfreview.data.local.GiphyDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user on 30-May-18.
 */
@Module
public class DbModule {

    @Provides
    @Singleton
    public GiphyDatabase provideDbModule(Context context) {
        return Room.databaseBuilder(context,
                GiphyDatabase.class, "database").build();
    }
}
