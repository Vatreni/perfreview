package com.anddevs.perfreview.presentation.di;

import com.anddevs.perfreview.presentation.trending.TrendingFragment;
import com.anddevs.perfreview.presentation.trending.TrendingViewModel;

import dagger.Component;

@TrendingScope
@Component(modules = TrendingModule.class, dependencies = AppComponent.class)
public interface TrendingComponent {
    void inject(TrendingFragment fragment);

    void inject(TrendingViewModel viewModel);
}
