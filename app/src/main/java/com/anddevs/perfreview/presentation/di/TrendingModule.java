package com.anddevs.perfreview.presentation.di;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import com.anddevs.perfreview.data.local.GiphyDatabase;
import com.anddevs.perfreview.data.remote.RestClient;
import com.anddevs.perfreview.presentation.base.ModelFactory;
import com.anddevs.perfreview.domain.TrendingRepository;
import com.anddevs.perfreview.presentation.trending.TrendingViewModel;
import dagger.Module;
import dagger.Provides;

@Module
public class TrendingModule {

    private Fragment fragment;

    public TrendingModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    public TrendingRepository provideRepository(RestClient client, GiphyDatabase database) {
        return new TrendingRepository(client, database);
    }

    @Provides
    public TrendingViewModel provideTrendingViewModel(TrendingRepository repository) {
        return ViewModelProviders.of(fragment, new ModelFactory(repository)).get(TrendingViewModel.class);
    }
}
