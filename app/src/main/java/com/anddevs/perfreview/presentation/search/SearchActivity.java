package com.anddevs.perfreview.presentation.search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import com.anddevs.perfreview.R;
import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.presentation.App;
import com.anddevs.perfreview.presentation.base.BaseActivity;
import com.anddevs.perfreview.presentation.DividerItemDecoration;
import com.anddevs.perfreview.presentation.di.DaggerSearchComponent;
import com.anddevs.perfreview.presentation.di.SearchModule;
import com.anddevs.perfreview.presentation.trending.TrendingAdapter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

public class SearchActivity extends BaseActivity {

    @Inject
    SearchViewModel viewModel;
    private TrendingAdapter adapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, SearchActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerSearchComponent.builder()
                .appComponent(App.get(getApplicationContext()).getAppComponent())
                .searchModule(new SearchModule(this))
                .build().inject(this);

        RecyclerView rvTrending = findViewById(R.id.rvTrending);
        rvTrending.addItemDecoration(new DividerItemDecoration(getResources().getDimensionPixelSize(R.dimen.divider)));
        rvTrending.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TrendingAdapter();
        rvTrending.setAdapter(adapter);

        initSearch();
    }

    @SuppressLint("CheckResult")
    private void initSearch() {
        EditText etSearch = findViewById(R.id.etSearch);
        Log.d("qqqqqqq", "initSearch: " + etSearch);
        RxTextView.textChanges(etSearch)
                .doOnNext(new Consumer<CharSequence>() {
                    @Override
                    public void accept(CharSequence charSequence) throws Exception {
                        Log.d("qqqqqqq", "accept: " + charSequence);

                    }
                })
                .filter(charSequence -> !TextUtils.isEmpty(charSequence) && charSequence.length() > 1)
                .debounce(400, TimeUnit.MILLISECONDS)
                .map(CharSequence::toString)
                .switchMap((Function<String, ObservableSource<List<Gif>>>) s -> viewModel.loadData(s))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    adapter.setData(data);
                }, Throwable::printStackTrace);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_search;
    }
}
