package com.anddevs.perfreview.domain;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;

import com.anddevs.perfreview.BuildConfig;
import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.data.local.GiphyDatabase;
import com.anddevs.perfreview.data.mapping.GifMapper;
import com.anddevs.perfreview.data.remote.RestClient;
import com.anddevs.perfreview.data.remote.models.Datum;
import com.anddevs.perfreview.data.remote.models.TrendingResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class TrendingRepository {

    private RestClient client;
    private GiphyDatabase db;

    public TrendingRepository(RestClient client, GiphyDatabase db) {
        this.client = client;
        this.db = db;
    }

    public LiveData<List<Gif>> getTrending() {
        loadTrendingFromNet();

        return db.gifDao().getAll();
    }

    @SuppressLint("CheckResult")
    public void loadTrendingFromNet() {
        Map<String, String> params = new HashMap<>();
        params.put("api_key", BuildConfig.API_KEY);
        params.put("limit", "20");
        params.put("offset", "0");
        params.put("rating", "g");
        params.put("fmt", "json");

        client.getApi().getData(params)
                .subscribeOn(Schedulers.io())
                .flatMapIterable((Function<TrendingResponse, Iterable<Datum>>) TrendingResponse::getData)
                .map(GifMapper::convertToGif)
                .toList()
                .subscribe(db.gifDao()::saveAll, Throwable::printStackTrace);
    }
}
