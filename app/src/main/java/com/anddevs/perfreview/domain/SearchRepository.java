package com.anddevs.perfreview.domain;


import android.util.Log;

import com.anddevs.perfreview.BuildConfig;
import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.data.local.GiphyDatabase;
import com.anddevs.perfreview.data.mapping.GifMapper;
import com.anddevs.perfreview.data.remote.RestClient;
import com.anddevs.perfreview.data.remote.models.Datum;
import com.anddevs.perfreview.data.remote.models.TrendingResponse;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class SearchRepository {

    private RestClient client;
    private GiphyDatabase db;

    public SearchRepository(RestClient client, GiphyDatabase db) {
        this.client = client;
        this.db = db;
    }

    public Observable<List<Gif>> search(String query) {
        Map<String, String> params = new HashMap<>();
        params.put("api_key", BuildConfig.API_KEY);
        params.put("q", query);
        params.put("limit", "20");
        params.put("offset", "0");
        params.put("rating", "g");
        params.put("fmt", "json");

        return client.getApi().search(params)
                .subscribeOn(Schedulers.io())
                .flatMapIterable((Function<TrendingResponse, Iterable<Datum>>) TrendingResponse::getData)
                .map(GifMapper::convertToGif)
                .toList()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> Log.d("qqqqq", "accept: " +throwable.getLocalizedMessage()));
    }

    private void searchFromNet() {

    }
}
