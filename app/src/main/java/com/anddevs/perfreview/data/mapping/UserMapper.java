package com.anddevs.perfreview.data.mapping;


import com.anddevs.perfreview.data.local.GifUser;
import com.anddevs.perfreview.data.remote.models.User;

public class UserMapper {

    public static GifUser convertToGifUser(User user) {
        GifUser gifUser = new GifUser();
        gifUser.avatarUrl = user.getAvatarUrl();
        gifUser.guid = user.getGuid();
        gifUser.username = user.getUsername();
        return gifUser;
    }
}
