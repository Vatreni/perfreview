package com.anddevs.perfreview.data.remote;

import com.anddevs.perfreview.data.remote.models.TrendingResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface RestApi {

    @GET("/v1/gifs/trending")
    Observable<TrendingResponse> getData(@QueryMap Map<String, String> params);

    @GET("/v1/gifs/search")
    Observable<TrendingResponse> search(@QueryMap Map<String, String> params);

}
