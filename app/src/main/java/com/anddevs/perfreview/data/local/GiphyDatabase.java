package com.anddevs.perfreview.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Gif.class, GifUser.class}, version = 1)
public abstract class GiphyDatabase extends RoomDatabase {
    public abstract GifDao gifDao();

    public abstract UserDao userDao();
}
