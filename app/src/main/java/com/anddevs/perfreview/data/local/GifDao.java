package com.anddevs.perfreview.data.local;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface GifDao {
    @Query("SELECT * FROM gif")
    LiveData<List<Gif>> getAll();

    @Query("SELECT * FROM gif WHERE title LIKE '%' || :query || '%'")
    List<Gif> getByQuery(String query);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void saveAll(List<Gif> gif);

    @Query("SELECT * FROM gif WHERE id = :id")
    LiveData<Gif> getById(long id);

    @Query("SELECT * FROM gif WHERE serverId = :id")
    Gif getByServerId(String id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Gif gif);

    @Update
    void update(Gif gif);

    @Delete
    void delete(Gif gif);
}
