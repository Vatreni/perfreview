package com.anddevs.perfreview.data.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = GifUser.class, parentColumns = "guid", childColumns = "userId", onDelete = CASCADE),
        indices = {@Index(value = {"serverId"}, unique = true)})
public class Gif {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public String serverId;
    public String title;
    public String downsizedUrl;
    public long date;
    public String userId;
}
