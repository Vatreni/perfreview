package com.anddevs.perfreview.data.local;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM GifUser")
    LiveData<List<GifUser>> getAll();

    @Query("SELECT * FROM GifUser WHERE id = :id")
    LiveData<GifUser> getById(long id);

    @Query("SELECT * FROM GifUser WHERE guid = :id")
    GifUser getByUserId(String id);

    @Query("SELECT * FROM GifUser WHERE username = :name")
    GifUser getByUserName(String name);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(GifUser user);

    @Update
    void update(GifUser user);

    @Delete
    void delete(GifUser user);
}
