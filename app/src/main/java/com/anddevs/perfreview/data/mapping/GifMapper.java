package com.anddevs.perfreview.data.mapping;


import android.util.Log;

import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.data.remote.models.Datum;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class GifMapper {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat OUTPUT_DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());

    public static Gif convertToGif(Datum datum) {
        Gif gif = new Gif();
        gif.downsizedUrl = datum.getImages().getDownsized().getUrl();
        gif.title = datum.getTitle();
        gif.serverId = datum.getId();
        //gif.userId = datum.getUser().getGuid();
        try {
            gif.date = parseDate(datum);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return gif;
    }

    public static long parseDate(Datum datum) throws ParseException {
        return DATE_FORMAT.parse(datum.getDateTime()).getTime();
    }
}
