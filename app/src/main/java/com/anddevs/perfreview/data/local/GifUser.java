package com.anddevs.perfreview.data.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(indices = {@Index(value = {"guid"}, unique = true)})
public class GifUser {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public String avatarUrl;
    public String guid;
    public String username;
}
