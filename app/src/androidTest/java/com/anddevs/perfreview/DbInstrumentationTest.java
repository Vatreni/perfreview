package com.anddevs.perfreview;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.data.local.GifDao;
import com.anddevs.perfreview.data.local.GifUser;
import com.anddevs.perfreview.data.local.GiphyDatabase;
import com.anddevs.perfreview.data.local.UserDao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class DbInstrumentationTest {

    private GiphyDatabase db;
    private UserDao userDao;
    private GifDao gifDao;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        db = Room.inMemoryDatabaseBuilder(context, GiphyDatabase.class).build();
        userDao = db.userDao();
        gifDao = db.gifDao();
    }

    @After
    public void closeDb() {
        db.close();
    }

    @Test
    public void writeAndReadUser() {
        GifUser gifUser = new GifUser();
        gifUser.avatarUrl = "https://plastichno.com/wp-content/uploads/2017/09/pp-759x500.jpg";
        gifUser.guid = "1234qwer";
        gifUser.username = "user1";
        userDao.insert(gifUser);

        GifUser user = userDao.getByUserId("1234qwer");
        assertThat(user.guid, equalTo(gifUser.guid));

        GifUser userByName = userDao.getByUserName("user1");
        assertThat(userByName.guid, equalTo(gifUser.guid));

    }

    @Test
    public void writeAndReadGif() {
        Gif gif = new Gif();
        gif.downsizedUrl = "https://i.gifer.com/7OhN.gif";
        gif.title = "gif";
        gif.serverId = "1234qwer";
        gif.date = System.currentTimeMillis();

        gifDao.insert(gif);

        Gif dbGif = gifDao.getByServerId("1234qwer");
        assertThat(gif.serverId, equalTo(dbGif.serverId));

    }
}
