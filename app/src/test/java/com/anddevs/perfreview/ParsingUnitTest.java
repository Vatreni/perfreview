package com.anddevs.perfreview;

import com.anddevs.perfreview.data.local.Gif;
import com.anddevs.perfreview.data.mapping.GifMapper;
import com.anddevs.perfreview.data.remote.models.Datum;

import org.junit.Test;

import java.text.ParseException;

public class ParsingUnitTest {

    @Test
    public void parseDatum() {
        Datum datum = new Datum();
        datum.setTitle("test");
        datum.setId("1234qwer");

        Gif gif  = GifMapper.convertToGif(datum);
        assert (gif != null);
    }

    @Test
    public void parseDate() throws ParseException {
        Datum datum = new Datum();
        datum.setDateTime("2019-06-22 01:23:10");
        long time = GifMapper.parseDate(datum);
        assert (time > 0);
    }
}
